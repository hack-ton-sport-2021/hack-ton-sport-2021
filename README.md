# **Hack-ton-Sport**

[![Pipeline Status][pipeline-status-badge]][pipeline-status-url]
[![Coverage Report][coverage-report-badge]][coverage-report-url]

## Introduction

The repository used during the Hack-ton-Sport 2021 hackathon organized by the HEIG-VD.

## Documentation

_Contains the instructions to develop this project on a local environment._

- [Apps documentation](apps/README.md)

### Deployment

_Contains the instructions to deploy this project on a production environment._

- [Deployment documentation](deployment/README.md)

## Contributing

Here is some help to get you started to contribute to the project:

1. Please start by reading our code of conduct available in the [`CODE_OF_CONDUCT.md`][code-of-conduct-url] file.
2. All contribution information is available in the [`CONTRIBUTING.md`][contributing-url] file.

Feel free to contribute to the project in any way that you can think of, your contributions are more than welcome!

[pipeline-status-badge]: https://gitlab.com/hack-ton-sport-2021/hack-ton-sport-2021/badges/master/pipeline.svg
[pipeline-status-url]: https://gitlab.com/hack-ton-sport-2021/hack-ton-sport-2021/-/pipelines?scope=branches&ref=master
[coverage-report-badge]: https://gitlab.com/hack-ton-sport-2021/hack-ton-sport-2021/badges/master/coverage.svg
[coverage-report-url]: https://gitlab.com/hack-ton-sport-2021/hack-ton-sport-2021/-/pipelines?scope=branches&ref=master
