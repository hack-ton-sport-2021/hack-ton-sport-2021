# **Hack-ton-Sport** - Deployment

- [Configure a custom virtual machine (VM)](virtual-machine/README.md)
- [Run the infrastructure with Docker Compose](docker-compose/README.md)
- [Configure the hardware](hardware/README.md)
- [Develop and deploy new versions on GitLab](gitlab/README.md)
