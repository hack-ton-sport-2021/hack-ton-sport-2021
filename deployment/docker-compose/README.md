# **Hack-ton-Sport** - Run the infrastructure with Docker Compose

## Introduction

Docker Compose is used to run the entire infrastructure.

## Prerequisites

The following prerequisites must be filled to run this project:

- [Docker](https://docs.docker.com/get-docker/) must be installed.
- [Docker Compose](https://docs.docker.com/compose/) must be installed.
- [git](https://git-scm.com/) must be installed.

## Clone the repository and configure the services

```sh
# Store git credentials indefinitely
git config credential.helper store

# Clone the repository
git clone https://gitlab.com/hack-ton-sport-2021/hack-ton-sport-2021.git hack-ton-sport-2021

# Move to the cloned directory
cd hack-ton-sport-2021/deployment/docker-compose

# Edit the environment variables file if needed
vim [<service>].env
```

## Start the infrastructure

Start the entire infrastructure.

```sh
# Move to the cloned directory
cd hack-ton-sport-2021/deployment/docker-compose

# Start the infrastructure
docker-compose --detach
```

## Stop the infrastructure

Stop the entire infrastructure.

```sh
# Move to the cloned directory
cd hack-ton-sport-2021/deployment/docker-compose

# Stop the infrastructure
docker-compose down
```

## Update the infrastructure

Fetch the latest available version of the Docker images from the remote registry.

```sh
# Move to the cloned directory
cd hack-ton-sport-2021/deployment/docker-compose

# Update the infrastructure
docker-compose pull
```

## Inspect the logs

Inspect the logs of the infrastructure.

```sh
# Move to the cloned directory
cd hack-ton-sport-2021/deployment/docker-compose

# Inspect the logs
docker-compose logs --follow
```

## Additional resources

- [Backend](../../apps/backend/README.md) - Documentation for the backend of Hack-ton-Sport.
- [Docker](https://docs.docker.com/get-docker/) - An open platform for developing, shipping, and running applications.
- [Docker Compose](https://docs.docker.com/compose/) - A tool for defining and running multi-container Docker applications.
- [Frontend](../../apps/frontend/README.md) - Documentation for the frontend of Hack-ton-Sport.
- [git](https://git-scm.com/) - a free and open source distributed version control system.
- [PostgreSQL](https://www.postgresql.org/) - The world's most advanced open source database.
- [Traefik](https://containo.us/traefik/) - Open-source Edge Router that makes publishing your services a fun and easy experience.
