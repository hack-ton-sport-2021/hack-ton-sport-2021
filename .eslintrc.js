module.exports = {
	env: {
		node: true,
		es6: true,
	},
	ignorePatterns: ['!.eslintrc.js', '!.prettierrc.js'],
	extends: ['eslint:recommended', 'plugin:prettier/recommended', 'prettier'],
	plugins: ['prettier'],
	rules: {
		indent: ['error', 'tab'],
		'no-tabs': 0,
	},
};
