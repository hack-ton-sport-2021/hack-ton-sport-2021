# **Hack-ton-sport** - Backend

## Prerequisites

The following prerequisites must be filled to run this project:

- [Node.js](https://nodejs.org/) must be installed.
- [pnpm](https://pnpm.js.org/) must be installed.

## Configuration

```sh
# Install the dependencies
pnpm install

# Copy the .env.defaults file to a .env file.
cp .env.defaults .env

# Edit the environment variables file if needed
vim .env
```

### Start the app in development mode

```sh
# Start the app in development mode
pnpm dev
```

### Build the app

```sh
# Build the app
pnpm build
```

### Start the app in production mode

```sh
# Start the app in production mode
pnpm start
```

## Additional resources

- [NestJS](https://nestjs.com/) - A progressive Node.js framework for building efficient, reliable and scalable server-side applications.
- [OpenAPI (Swagger)](https://swagger.io/specification/) - Defines a standard, language-agnostic interface to RESTful APIs.
- [Prisma](https://www.prisma.io/) - Replace traditional ORMs and makes database access easy with an auto-generated query builder for TypeScript & Node.js.
- [TypeScript](https://www.typescriptlang.org/) - An open-source language which builds on JavaScript by adding static type definitions.
