import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { bootstrap } from './bootstrap';
import { PORT } from './constants';

NestFactory.create(AppModule)
	.then((app) => bootstrap(app))
	.then((app) => app.listen(PORT));
