import { Injectable, NotFoundException } from '@nestjs/common';
import {
	Prisma,
	ResetPasswordRequest,
	User as UserModel,
	UserStatus,
} from '@prisma/client';
import { hash } from 'argon2';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class PasswordsService {
	constructor(readonly prisma: PrismaService) {}

	async findFirst(
		where: Prisma.ResetPasswordRequestWhereInput,
	): Promise<ResetPasswordRequest> {
		const resetPasswordRequest = await this.prisma.resetPasswordRequest.findFirst(
			{
				where,
			},
		);

		if (resetPasswordRequest) {
			return resetPasswordRequest;
		}

		throw new NotFoundException();
	}

	async request(email: string): Promise<string> {
		const user = await this.prisma.user.findFirst({
			where: {
				email,
			},
		});

		if (user) {
			const request = await this.prisma.resetPasswordRequest.upsert({
				create: {
					userId: user.id,
				},
				update: {},
				where: {
					userId: user.id,
				},
			});

			return request.id;
		}

		throw new NotFoundException();
	}

	async reset(user: UserModel, password: string): Promise<void> {
		await this.prisma.resetPasswordRequest.delete({
			where: { userId: user.id },
		});

		await this.prisma.user.update({
			where: {
				id: user.id,
			},
			data: {
				password: await hash(password),
				status: UserStatus.ENABLED,
			},
		});
	}

	async delete(params: {
		where: Prisma.ResetPasswordRequestWhereUniqueInput;
	}): Promise<ResetPasswordRequest> {
		const { where } = params;

		// TODO: Improve when the following issues are resolved
		// https://github.com/prisma/prisma/issues/2328
		// https://github.com/prisma/prisma/issues/2057
		// Docs: https://www.prisma.io/docs/concepts/components/prisma-client/raw-database-access#queryraw
		return this.prisma.$queryRaw<ResetPasswordRequest>(
			'DELETE FROM reset_password_requests WHERE id = $1',
			where.id,
		);

		// return this.prisma.resetPasswordRequest.delete({
		// 	where,
		// });
	}
}
