import { Module } from '@nestjs/common';
import { PrismaModule } from '../prisma/prisma.module';
import { PasswordsService } from './passwords.service';

@Module({
	imports: [PrismaModule],
	providers: [PasswordsService],
	exports: [PasswordsService],
})
export class PasswordsModule {}
