import { Module } from '@nestjs/common';
import { ConfigModule as ConfigurationModule } from '@nestjs/config';
import Joi from 'joi';

@Module({
	imports: [
		ConfigurationModule.forRoot({
			validationSchema: Joi.object({
				HACK_TON_SPORT_BACKEND_VERSION: Joi.string(),
				HACK_TON_SPORT_BACKEND_FQDN: Joi.string(),
				HACK_TON_SPORT_FRONTEND_FQDN: Joi.string(),
				HACK_TON_SPORT_BACKEND_DEFAULT_ADMIN_FIRST_NAME: Joi.string(),
				HACK_TON_SPORT_BACKEND_DEFAULT_ADMIN_LAST_NAME: Joi.string(),
				HACK_TON_SPORT_BACKEND_DEFAULT_ADMIN_EMAIL: Joi.string(),
				HACK_TON_SPORT_BACKEND_DEFAULT_ADMIN_PASSWORD: Joi.string(),
				HACK_TON_SPORT_BACKEND_DEFAULT_ADMIN_LANGUAGE: Joi.string(),
				HACK_TON_SPORT_BACKEND_DEFAULT_ADMIN_BIRTH_DATE: Joi.string(),
				HACK_TON_SPORT_BACKEND_DEFAULT_ADMIN_GENDER: Joi.string(),
				HACK_TON_SPORT_BACKEND_DEFAULT_ADMIN_MAIN_SPORT: Joi.string(),
				HACK_TON_SPORT_BACKEND_JWT_SECRET: Joi.string(),
				HACK_TON_SPORT_BACKEND_JWT_EXPIRATION_TIME: Joi.string(),
				HACK_TON_SPORT_BACKEND_JWT_ISSUER: Joi.string(),
				HACK_TON_SPORT_BACKEND_MAIL_HOST: Joi.string(),
				HACK_TON_SPORT_BACKEND_MAIL_PORT: Joi.number(),
				HACK_TON_SPORT_BACKEND_MAIL_SECURE: Joi.boolean(),
				HACK_TON_SPORT_BACKEND_MAIL_USERNAME: Joi.string().allow(''),
				HACK_TON_SPORT_BACKEND_MAIL_PASSWORD: Joi.string().allow(''),
				HACK_TON_SPORT_BACKEND_MAIL_ADMIN_NAME: Joi.string(),
				HACK_TON_SPORT_BACKEND_MAIL_ADMIN_EMAIL: Joi.string(),
				HACK_TON_SPORT_BACKEND_MAIL_NO_REPLY_NAME: Joi.string(),
				HACK_TON_SPORT_BACKEND_MAIL_NO_REPLY_EMAIL: Joi.string(),
				HACK_TON_SPORT_BACKEND_I18N_FALLBACK_LANGUAGE: Joi.string(),
			}),
			validationOptions: {
				allowUnknown: true,
				abortEarly: false,
			},
		}),
	],
	exports: [ConfigurationModule],
})
export class ConfigModule {}
