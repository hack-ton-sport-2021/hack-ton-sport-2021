import { Module, forwardRef } from '@nestjs/common';
import { I18nJsonParser, I18nModule } from 'nestjs-i18n';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { ConfigModule, ConfigService } from '@nestjs/config';
import path from 'path';
import { JwtModule } from '@nestjs/jwt';
import { MailService } from './mail.service';
import { UsersModule } from '../users/users.module';
import {
	I18N_FALLBACK_LANGUAGE,
	JWT_EXPIRATION_TIME,
	JWT_SECRET,
	MAIL_ADMIN_EMAIL,
	MAIL_ADMIN_NAME,
	MAIL_HOST,
	MAIL_NO_REPLY_EMAIL,
	MAIL_NO_REPLY_NAME,
	MAIL_PASSWORD,
	MAIL_PORT,
	MAIL_SECURE,
	MAIL_USERNAME,
} from '../constants';

@Module({
	imports: [
		ConfigModule,
		I18nModule.forRootAsync({
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: async (config: ConfigService) => ({
				fallbackLanguage: config.get<string>(I18N_FALLBACK_LANGUAGE) as string,
				parserOptions: {
					path: path.join(__dirname, '/locales/'),
				},
			}),
			parser: I18nJsonParser,
		}),
		JwtModule.registerAsync({
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: async (config: ConfigService) => ({
				secret: config.get<string>(JWT_SECRET),
				signOptions: {
					expiresIn: config.get<string>(JWT_EXPIRATION_TIME),
				},
			}),
		}),
		MailerModule.forRootAsync({
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: async (config: ConfigService) => {
				const host = config.get<string>(MAIL_HOST);
				const port = config.get<number>(MAIL_PORT);
				const secure = config.get<boolean>(MAIL_SECURE);
				const user = config.get<string>(MAIL_USERNAME);
				const password = config.get<string>(MAIL_PASSWORD);
				const from = `"${config.get<string>(
					MAIL_NO_REPLY_NAME,
				)}" <${config.get<string>(MAIL_NO_REPLY_EMAIL)}>`;
				const to = `"${config.get<string>(
					MAIL_ADMIN_NAME,
				)}" <${config.get<string>(MAIL_ADMIN_EMAIL)}>`;

				let auth;

				if (user && password) {
					auth = {
						user,
						pass: password,
					};
				}

				return {
					transport: {
						host,
						port,
						secure,
						auth,
						tls: {
							rejectUnauthorized: false,
						},
					},
					defaults: {
						from,
						to,
					},
					template: {
						dir: path.join(__dirname, '/templates'),
						adapter: new HandlebarsAdapter(),
						options: {
							strict: true,
						},
					},
				};
			},
		}),
		forwardRef(() => UsersModule),
	],
	providers: [MailService],
	exports: [MailService],
})
export class MailModule {}
