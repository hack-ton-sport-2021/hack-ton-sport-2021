import { MailerService } from '@nestjs-modules/mailer';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { User, UserRole, UserStatus } from '@prisma/client';
import { I18nService } from 'nestjs-i18n';
import { FRONTEND_FQDN } from '../constants';
import { UsersService } from '../users/users.service';

@Injectable()
export class MailService implements OnModuleInit {
	constructor(
		readonly config: ConfigService,
		readonly i18n: I18nService,
		readonly jwt: JwtService,
		readonly mailer: MailerService,
		readonly users: UsersService,
	) {}

	async onModuleInit(): Promise<void> {
		const adminUsers = await this.users.findMany({
			where: { role: UserRole.ADMIN, status: UserStatus.ENABLED },
		});

		await Promise.all(
			adminUsers.items.map(async (adminUser) => {
				const lang = adminUser.language.toLowerCase();

				const subject = await this.i18n.translate(
					'mail.serviceStarted.subject',
					{
						lang,
					},
				);

				const content = await this.i18n.translate(
					'mail.serviceStarted.content',
					{
						lang,
					},
				);

				await this.mailer.sendMail({
					subject,
					to: adminUser.email,
					template: './service-started',
					context: {
						subject,
						content,
					},
				});
			}),
		);
	}

	async sendAccountCreated(user: User, resetPasswordId: string): Promise<void> {
		const lang = user.language.toLowerCase();

		const subject = await this.i18n.translate('mail.accountCreated.subject', {
			lang,
		});

		const greetings = await this.i18n.translate(
			'mail.accountCreatedGreetings',
			{
				lang,
				args: {
					firstName: user.firstName,
					lastName: user.lastName,
				},
			},
		);

		const content = await this.i18n.translate('mail.accountCreated.content', {
			lang,
		});

		const endings = await this.i18n.translate('mail.accountCreated.endings', {
			lang,
		});

		const url = `https://${this.config.get<string>(
			FRONTEND_FQDN,
		)}/auth/reset-password?requestId=${resetPasswordId}`;

		await this.mailer.sendMail({
			subject,
			to: user.email,
			template: './account-created',
			context: {
				subject,
				greetings,
				content,
				url,
				endings,
			},
		});
	}

	async sendResetPasswordRequest(user: User, requestId: string): Promise<void> {
		const lang = user.language.toLowerCase();

		const subject = await this.i18n.translate(
			'mail.resetPasswordRequest.subject',
			{
				lang,
			},
		);

		const greetings = await this.i18n.translate(
			'mail.resetPasswordRequest.greetings',
			{
				lang,
				args: {
					firstName: user.firstName,
					lastName: user.lastName,
				},
			},
		);

		const content = await this.i18n.translate(
			'mail.resetPasswordRequest.content',
			{
				lang,
			},
		);

		const endings = await this.i18n.translate(
			'mail.resetPasswordRequest.endings',
			{
				lang,
			},
		);

		const url = `https://${this.config.get<string>(
			FRONTEND_FQDN,
		)}/auth/reset-password?requestId=${requestId}`;

		await this.mailer.sendMail({
			subject,
			to: user.email,
			template: './reset-password-request',
			context: {
				subject,
				greetings,
				content,
				url,
				endings,
			},
		});
	}
}
