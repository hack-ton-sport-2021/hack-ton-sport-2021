import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { Pagination, PaginationCursors, PaginationMeta } from './interfaces';

export class PaginationMetaDto implements PaginationMeta {
	/**
	 * the total number of results
	 */
	numberOfResults!: number;
}

export class PaginationCursorsDto implements PaginationCursors {
	/**
	 * the link to access the previous items
	 */
	@ApiPropertyOptional()
	@IsString()
	previous: string | null;

	/**
	 * the link to access the next items
	 */
	@ApiPropertyOptional()
	@IsString()
	next: string | null;

	constructor(previous: string, next: string) {
		this.previous = previous;
		this.next = next;
	}
}

export abstract class PaginationDto<T> implements Pagination<T> {
	items: T[];

	meta: PaginationMetaDto;

	/**
	 * the cursors associated to the pagination
	 */
	cursors: PaginationCursorsDto;

	constructor(
		items: T[],
		meta: PaginationMetaDto,
		cursors: PaginationCursorsDto,
	) {
		this.items = items;
		this.meta = meta;
		this.cursors = cursors;
	}

	// eslint-disable-next-line class-methods-use-this
	static createPaginationCursors(
		cursors: PaginationCursors,
		take: number,
		queryParams: Map<string, string[]>,
	): PaginationCursors {
		const previousQueryParams = queryParams;
		const nextQueryParams = queryParams;

		const previous: string[] = [];
		const next: string[] = [];

		if (cursors.previous) {
			previousQueryParams.set('cursor', [cursors.previous]);
			previousQueryParams.set('take', [`-${Math.abs(take || 0)}`]);

			queryParams.forEach((value, key) => {
				previous.push(
					`${key}=${value.map((item) => encodeURIComponent(item))}`,
				);
			});
		}

		if (cursors.next) {
			nextQueryParams.set('cursor', [cursors.next]);
			nextQueryParams.set('take', [`${Math.abs(take || 0)}`]);

			queryParams.forEach((value, key) => {
				next.push(`${key}=${value.map((item) => encodeURIComponent(item))}`);
			});
		}

		return {
			previous: cursors.previous ? previous.join('&') : null,
			next: cursors.next ? next.join('&') : null,
		};
	}
}
