import {
	CallHandler,
	ExecutionContext,
	Injectable,
	NestInterceptor,
} from '@nestjs/common';
import { UserRole, User as UserModel } from '@prisma/client';
import { Observable } from 'rxjs';
import { UsersService } from '../users/users.service';

@Injectable()
export class SwitchUser implements NestInterceptor {
	constructor(readonly users: UsersService) {}

	async intercept(
		context: ExecutionContext,
		next: CallHandler,
	): Promise<Observable<unknown>> {
		const request = context.switchToHttp().getRequest();

		const user = request.user as UserModel;

		if (user) {
			const currentUser = user;
			const forUser: string = request.query.user;

			if (
				forUser &&
				currentUser?.id !== forUser &&
				currentUser?.role === UserRole.ADMIN
			) {
				const user = await this.users.findFirst({
					where: {
						id: forUser,
					},
				});

				request.user = user;
			}
		}

		return next.handle();
	}
}
