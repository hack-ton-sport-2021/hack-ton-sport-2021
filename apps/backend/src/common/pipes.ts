import {
	HttpStatus,
	Injectable,
	Optional,
	PipeTransform,
} from '@nestjs/common';
import {
	ErrorHttpStatusCode,
	HttpErrorByCode,
} from '@nestjs/common/utils/http-error-by-code.util';

const CUSTOM_PARSE_INT_VALIDATION_ERROR_MESSAGE =
	'Validation failed (numeric string is expected)';

export interface CustomParseIntPipeOptions {
	optional?: boolean;
	errorHttpStatusCode?: ErrorHttpStatusCode;
	exceptionFactory?: (error: string) => unknown;
}

@Injectable()
export class CustomParseIntPipe implements PipeTransform<number> {
	protected exceptionFactory: (error: string) => unknown;

	constructor(
		@Optional() private readonly options: CustomParseIntPipeOptions = {},
	) {
		const {
			exceptionFactory,
			errorHttpStatusCode = HttpStatus.BAD_REQUEST,
		} = options;

		this.exceptionFactory =
			exceptionFactory ||
			((error) => new HttpErrorByCode[errorHttpStatusCode](error));
	}

	async transform(value: number): Promise<number | undefined> {
		const isNumeric =
			['string', 'number'].includes(typeof value) &&
			!Number.isNaN(value) &&
			Number.isFinite(value as unknown);

		if (!isNumeric && !this.options.optional) {
			throw this.exceptionFactory(CUSTOM_PARSE_INT_VALIDATION_ERROR_MESSAGE);
		} else if (!isNumeric && this.options.optional) {
			return undefined;
		}

		return value;
	}
}

const PARSE_DATE_VALIDATION_ERROR_MESSAGE =
	'Validation failed (date string is expected)';

export interface CustomParseDatePipeOptions {
	optional?: boolean;
	errorHttpStatusCode?: ErrorHttpStatusCode;
	exceptionFactory?: (error: string) => unknown;
}

@Injectable()
export class CustomParseDatePipe implements PipeTransform<Date> {
	protected exceptionFactory: (error: string) => unknown;

	constructor(
		@Optional() private readonly options: CustomParseDatePipeOptions = {},
	) {
		const {
			exceptionFactory,
			errorHttpStatusCode = HttpStatus.BAD_REQUEST,
		} = options;

		this.exceptionFactory =
			exceptionFactory ||
			((error) => new HttpErrorByCode[errorHttpStatusCode](error));
	}

	async transform(value: Date): Promise<Date | undefined> {
		if (
			(!value || value.toString() === 'Invalid Date') &&
			!this.options.optional
		) {
			throw this.exceptionFactory(PARSE_DATE_VALIDATION_ERROR_MESSAGE);
		} else if (
			(!value || value.toString() === 'Invalid Date') &&
			this.options.optional
		) {
			return undefined;
		}

		const isDate = value.toString() !== 'Invalid Date';

		if (!isDate) {
			throw this.exceptionFactory(PARSE_DATE_VALIDATION_ERROR_MESSAGE);
		}

		return value;
	}
}

export interface CustomParseBoolPipeOptions {
	optional?: boolean;
	errorHttpStatusCode?: ErrorHttpStatusCode;
	exceptionFactory?: (error: string) => unknown;
}

@Injectable()
export class CustomParseBoolPipe
	implements PipeTransform<string | boolean, Promise<boolean | undefined>> {
	protected exceptionFactory: (error: string) => unknown;

	constructor(
		@Optional() private readonly options: CustomParseDatePipeOptions = {},
	) {
		const {
			exceptionFactory,
			errorHttpStatusCode = HttpStatus.BAD_REQUEST,
		} = options;

		this.exceptionFactory =
			exceptionFactory ||
			((error) => new HttpErrorByCode[errorHttpStatusCode](error));
	}

	async transform(value: string | boolean): Promise<boolean | undefined> {
		if (value === true || value === 'true') {
			return true;
		}
		if (value === false || value === 'false') {
			return false;
		}
		if (this.options.optional) {
			return undefined;
		}

		throw this.exceptionFactory(
			'Validation failed (boolean string is expected)',
		);
	}
}
