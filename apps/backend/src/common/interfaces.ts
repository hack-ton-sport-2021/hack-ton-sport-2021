export interface PaginationMeta {
	numberOfResults: number;
}

export interface PaginationCursors {
	previous: string | null;
	next: string | null;
}

export interface Pagination<T> {
	items: T[];
	meta: PaginationMeta;
	cursors: PaginationCursors;
}
