import { ApiResponseOptions } from '@nestjs/swagger';

export const InvalidQueriesValuesResponse: ApiResponseOptions = {
	description:
		'Some queries values are missing or incorrect. Please check your inputs.',
};

export const MissingOrIncorrectFieldsResponse: ApiResponseOptions = {
	description:
		'Some fields are missing or incorrect. Please check your inputs.',
};
