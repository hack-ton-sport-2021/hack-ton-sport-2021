import { ApiQueryOptions } from '@nestjs/swagger';
import { UserGender, UserStatus } from '@prisma/client';

export const CursorQuery: ApiQueryOptions = {
	name: 'cursor',
	description: 'The cursor from where to get the items.',
	required: false,
	schema: {
		type: 'string',
	},
};

export const TakeQuery: ApiQueryOptions = {
	name: 'take',
	description: 'The number of items to take.',
	required: false,
	schema: {
		type: 'integer',
	},
};

export const AllQuery = (...roles: string[]): ApiQueryOptions => ({
	name: 'all',
	description: `Take all entities stored in the database, even those not related to the current user (only for roles: ${roles.join(
		',',
	)})`,
	required: false,
	schema: {
		type: 'boolean',
	},
});

export const UserQuery: ApiQueryOptions = {
	name: 'user',
	description: 'Execute the request for another user (admin only)',
	required: false,
	schema: {
		type: 'string',
		format: 'uuid',
	},
};

export const FilterByFirstNameQuery: ApiQueryOptions = {
	name: 'firstName',
	description: 'Filter the data by first name',
	required: false,
	schema: {
		type: 'array',
		items: {
			type: 'string',
		},
	},
	explode: false,
	style: 'form',
};

export const FilterByLastNameQuery: ApiQueryOptions = {
	name: 'lastName',
	description: 'Filter the data by last name',
	required: false,
	schema: {
		type: 'array',
		items: {
			type: 'string',
		},
	},
	explode: false,
	style: 'form',
};

export const FilterByEmailQuery: ApiQueryOptions = {
	name: 'email',
	description: 'Filter the data by email',
	required: false,
	schema: {
		type: 'array',
		items: {
			type: 'string',
		},
	},
	explode: false,
	style: 'form',
};

export const FilterByUserGenderQuery: ApiQueryOptions = {
	name: 'userGender',
	description: 'Filter the data by user gender',
	required: false,
	schema: {
		type: 'array',
		items: {
			type: 'string',
			enum: [...Object.values(UserGender)],
		},
	},
	explode: false,
	style: 'form',
};

export const FilterByUserStatusQuery: ApiQueryOptions = {
	name: 'userStatus',
	description: 'Filter the data by user status',
	required: false,
	schema: {
		type: 'array',
		items: {
			type: 'string',
			enum: [...Object.values(UserStatus)],
		},
	},
	explode: false,
	style: 'form',
};

export const FilterByNameQuery: ApiQueryOptions = {
	name: 'name',
	description: 'Filter the data by name',
	required: false,
	schema: {
		type: 'array',
		items: {
			type: 'string',
		},
	},
	explode: false,
	style: 'form',
};

export const FilterByDescriptionQuery: ApiQueryOptions = {
	name: 'description',
	description: 'Filter the data by description',
	required: false,
	schema: {
		type: 'array',
		items: {
			type: 'string',
		},
	},
	explode: false,
	style: 'form',
};
