import { ExecutionContext, createParamDecorator } from '@nestjs/common';
import { User as UserModel } from '@prisma/client';
import { Request } from 'express';

export const User = createParamDecorator(
	(data: string, ctx: ExecutionContext): UserModel => {
		const request: Request = ctx.switchToHttp().getRequest();

		const user = request.user as UserModel;

		return user as UserModel;
	},
);
