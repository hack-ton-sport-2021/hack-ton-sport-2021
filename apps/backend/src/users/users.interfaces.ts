import { Language, UserGender, UserRole, UserStatus } from '@prisma/client';

export type AbstractUser = {
	readonly id: string;
	readonly firstName: string;
	readonly lastName: string;
	readonly email: string;
	readonly password: string;
	readonly language: Language;
	readonly birthDate: Date;
	readonly gender: UserGender;
	readonly mainSport: string;
	readonly role: UserRole;
	readonly status: UserStatus;
	readonly createdAt: Date;
	readonly updatedAt: Date;
};

export type User = Omit<AbstractUser, 'password'>;

export type CreateUser = Omit<
	AbstractUser,
	'id' | 'password' | 'status' | 'createdAt' | 'updatedAt'
>;

export type UpdateUser = Omit<
	AbstractUser,
	'id' | 'password' | 'status' | 'createdAt' | 'updatedAt'
>;

export type UserSubset = Pick<
	AbstractUser,
	'id' | 'firstName' | 'lastName' | 'email' | 'mainSport' | 'role' | 'status'
>;
