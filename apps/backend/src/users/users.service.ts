import { Injectable, NotFoundException, OnModuleInit } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
	Language,
	Prisma,
	User,
	UserGender,
	UserRole,
	UserStatus,
} from '@prisma/client';
import { hash } from 'argon2';
import {
	DEFAULT_ADMIN_BIRTH_DATE,
	DEFAULT_ADMIN_EMAIL,
	DEFAULT_ADMIN_FIRST_NAME,
	DEFAULT_ADMIN_GENDER,
	DEFAULT_ADMIN_LANGUAGE,
	DEFAULT_ADMIN_LAST_NAME,
	DEFAULT_ADMIN_MAIN_SPORT,
	DEFAULT_ADMIN_PASSWORD,
} from '../constants';
import { Pagination } from '../common/interfaces';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class UsersService implements OnModuleInit {
	readonly include: Prisma.UserInclude | undefined = undefined;

	constructor(readonly config: ConfigService, readonly prisma: PrismaService) {}

	async onModuleInit(): Promise<void> {
		// Create the default user
		const firstName = this.config.get<string>(
			DEFAULT_ADMIN_FIRST_NAME,
		) as string;
		const lastName = this.config.get<string>(DEFAULT_ADMIN_LAST_NAME) as string;
		const email = this.config.get<string>(DEFAULT_ADMIN_EMAIL) as string;
		const password = await hash(
			this.config.get<string>(DEFAULT_ADMIN_PASSWORD) as string,
		);
		const language = this.config.get<Language>(
			DEFAULT_ADMIN_LANGUAGE,
		) as Language;
		const birthDate = this.config.get<string>(
			DEFAULT_ADMIN_BIRTH_DATE,
		) as string;
		const gender = this.config.get<UserGender>(
			DEFAULT_ADMIN_GENDER,
		) as UserGender;
		const mainSport = this.config.get<string>(
			DEFAULT_ADMIN_MAIN_SPORT,
		) as string;

		try {
			await this.create({
				data: {
					firstName,
					lastName,
					email,
					password,
					language,
					birthDate: new Date(birthDate),
					gender,
					mainSport,
					role: UserRole.ADMIN,
					status: UserStatus.ENABLED,
				},
			});
		} catch (error) {
			// Do nothing
		}
	}

	async findMany(params: {
		skip?: number;
		take?: number;
		cursor?: Prisma.UserWhereUniqueInput;
		where?: Prisma.UserWhereInput;
		orderBy?: Prisma.Enumerable<Prisma.UserOrderByInput>;
	}): Promise<Pagination<User>> {
		const { skip, take, cursor, where, orderBy } = params;

		const [numberOfResults, items] = await this.prisma.$transaction([
			this.prisma.user.count({
				where,
			}),
			this.prisma.user.findMany({
				skip,
				take,
				cursor,
				where,
				orderBy,
				include: this.include,
			}),
		]);
		const { 0: first, [items.length - 1]: last } = items;

		const previousItem = await this.prisma.user.findFirst({
			skip: 1,
			take: -1,
			cursor: first ? { id: first.id } : cursor,
			where,
			orderBy,
		});

		const nextItem = await this.prisma.user.findFirst({
			skip: 1,
			take: 1,
			cursor: last ? { id: last.id } : cursor,
			where,
			orderBy,
		});

		let previous: string | null = first?.id;
		let next: string | null = last?.id;

		if (!previousItem) {
			previous = null;
		}

		if (!nextItem) {
			next = null;
		}

		return {
			items,
			meta: {
				numberOfResults,
			},
			cursors: {
				previous,
				next,
			},
		};
	}

	async create(params: { data: Prisma.UserCreateInput }): Promise<User> {
		const { data } = params;

		return this.prisma.user.create({
			data,
			include: this.include,
		});
	}

	async findFirst(params: { where: Prisma.UserWhereInput }): Promise<User> {
		const { where } = params;

		const user = await this.prisma.user.findFirst({
			where,
			include: this.include,
		});

		if (user) {
			return user;
		}

		throw new NotFoundException();
	}

	async update(params: {
		where: Prisma.UserWhereUniqueInput;
		data: Prisma.UserUpdateInput;
	}): Promise<User> {
		const { where, data } = params;

		return this.prisma.user.update({
			data,
			where,
			include: this.include,
		});
	}

	async delete(params: { where: Prisma.UserWhereUniqueInput }): Promise<User> {
		const { where } = params;

		// TODO: Improve when the following issues are resolved
		// https://github.com/prisma/prisma/issues/2328
		// https://github.com/prisma/prisma/issues/2057
		// Docs: https://www.prisma.io/docs/concepts/components/prisma-client/raw-database-access#queryraw
		return this.prisma.$queryRaw<User>(
			'DELETE FROM users WHERE id = $1',
			where.id,
		);

		// return this.prisma.user.delete({
		// 	where,
		// 	include: this.include,
		// });
	}
}
