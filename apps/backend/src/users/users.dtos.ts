import {
	ApiProperty,
	ApiPropertyOptional,
	getSchemaPath,
	OmitType,
	PickType,
} from '@nestjs/swagger';
import { Language, UserGender, UserRole, UserStatus } from '@prisma/client';
import {
	IsDateString,
	IsEmail,
	IsEnum,
	IsNumber,
	IsString,
	IsUUID,
	Min,
	MinLength,
} from 'class-validator';
import {
	PaginationCursorsDto,
	PaginationDto,
	PaginationMetaDto,
} from '../common/dtos';
import {
	AbstractUser,
	CreateUser,
	UpdateUser,
	User,
	UserSubset,
} from './users.interfaces';
import { MININUM_PASSWORD_LENGTH } from '../constants';
import { Pagination } from '../common/interfaces';

export class AbstractUserDto implements AbstractUser {
	/**
	 * id of the user
	 */
	@ApiProperty({ format: 'uuid' })
	@IsUUID()
	id!: string;

	/**
	 * first name of the user
	 */
	@IsString()
	@MinLength(1)
	firstName!: string;

	/**
	 * last name of the user
	 */
	@IsString()
	@MinLength(1)
	lastName!: string;

	/**
	 * email of the user
	 */
	@ApiProperty({ format: 'email' })
	@IsEmail()
	email!: string;

	/**
	 * password of the user
	 */
	@ApiProperty({ format: 'password' })
	@IsString()
	@MinLength(MININUM_PASSWORD_LENGTH)
	password!: string;

	/**
	 * language of the user
	 */
	@ApiProperty({ enum: Language })
	@IsEnum(Language)
	language!: Language;

	/**
	 * when the user was born
	 */
	@IsDateString()
	birthDate!: Date;

	/**
	 * gender of the user
	 */
	@ApiProperty({ enum: UserGender })
	@IsEnum(UserGender)
	gender!: UserGender;

	/**
	 * main sport of the user
	 */
	@IsString()
	@MinLength(1)
	mainSport!: string;

	/**
	 * role of the user
	 */
	@ApiProperty({ enum: UserRole })
	@IsEnum(UserRole)
	role!: UserRole;

	/**
	 * status of the user
	 */
	@ApiProperty({ enum: UserStatus })
	@IsEnum(UserStatus)
	status!: UserStatus;

	/**
	 * when the entity was created
	 */
	@IsDateString()
	createdAt!: Date;

	/**
	 * when the entity was last updated
	 */
	@IsDateString()
	updatedAt!: Date;
}

export class UserDto
	extends OmitType(AbstractUserDto, ['password'] as const)
	implements User {
	constructor(
		readonly id: string,
		readonly firstName: string,
		readonly lastName: string,
		readonly email: string,
		readonly language: Language,
		readonly birthDate: Date,
		readonly gender: UserGender,
		readonly mainSport: string,
		readonly role: UserRole,
		readonly status: UserStatus,
		readonly createdAt: Date,
		readonly updatedAt: Date,
	) {
		super();
	}

	static toDto(user: User): UserDto {
		return new this(
			user.id,
			user.firstName,
			user.lastName,
			user.email,
			user.language,
			user.birthDate,
			user.gender,
			user.mainSport,
			user.role,
			user.status,
			user.createdAt,
			user.updatedAt,
		);
	}
}

export class CreateUserDto
	extends OmitType(AbstractUserDto, [
		'id',
		'password',
		'status',
		'createdAt',
		'updatedAt',
	] as const)
	implements CreateUser {
	constructor(
		readonly firstName: string,
		readonly lastName: string,
		readonly email: string,
		readonly language: Language,
		readonly birthDate: Date,
		readonly gender: UserGender,
		readonly mainSport: string,
		readonly role: UserRole,
	) {
		super();
	}
}

export class UpdateUserDto
	extends OmitType(AbstractUserDto, [
		'id',
		'password',
		'status',
		'createdAt',
		'updatedAt',
	] as const)
	implements UpdateUser {
	constructor(
		readonly firstName: string,
		readonly lastName: string,
		readonly email: string,
		readonly language: Language,
		readonly birthDate: Date,
		readonly gender: UserGender,
		readonly mainSport: string,
		readonly role: UserRole,
	) {
		super();
	}
}

export class UserSubsetDto
	extends PickType(AbstractUserDto, [
		'id',
		'firstName',
		'lastName',
		'email',
		'mainSport',
		'role',
		'status',
	] as const)
	implements UserSubset {
	constructor(
		readonly id: string,
		readonly firstName: string,
		readonly lastName: string,
		readonly email: string,
		readonly mainSport: string,
		readonly role: UserRole,
		readonly status: UserStatus,
	) {
		super();
	}

	static toDto(user: UserSubset): UserSubsetDto {
		return new this(
			user.id,
			user.firstName,
			user.lastName,
			user.email,
			user.mainSport,
			user.role,
			user.status,
		);
	}
}

export class UserPaginationDto extends PaginationDto<UserSubsetDto> {
	/**
	 * the users
	 */
	items!: UserSubsetDto[];

	constructor(
		items: UserSubsetDto[],
		meta: PaginationMetaDto,
		cursors: PaginationCursorsDto,
	) {
		super(items, meta, cursors);
	}

	static toDto(
		pagination: Pagination<User>,
		take: number,
		queryParams: Map<string, string[]>,
	): UserPaginationDto {
		return new this(
			pagination.items.map((item) => UserSubsetDto.toDto(item)),
			pagination.meta,
			super.createPaginationCursors(pagination.cursors, take, queryParams),
		);
	}
}
