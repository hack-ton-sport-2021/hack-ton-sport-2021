import {
	Body,
	Controller,
	Delete,
	Get,
	HttpCode,
	Param,
	ParseArrayPipe,
	ParseUUIDPipe,
	Patch,
	Post,
	Query,
} from '@nestjs/common';
import {
	ApiBadRequestResponse,
	ApiBody,
	ApiConflictResponse,
	ApiCreatedResponse,
	ApiNoContentResponse,
	ApiNotFoundResponse,
	ApiOkResponse,
	ApiOperation,
	ApiParam,
	ApiQuery,
	ApiTags,
} from '@nestjs/swagger';
import { Prisma, UserRole, UserStatus } from '@prisma/client';
import { nanoid } from 'nanoid';
import { hash } from 'argon2';
import { Pagination } from '../common/interfaces';
import { MailService } from '../mail/mail.service';
import { CustomParseIntPipe } from '../common/pipes';
import {
	CursorQuery,
	FilterByEmailQuery,
	FilterByFirstNameQuery,
	FilterByLastNameQuery,
	FilterByUserStatusQuery,
	TakeQuery,
} from '../common/swagger/queries';
import {
	InvalidQueriesValuesResponse,
	MissingOrIncorrectFieldsResponse,
} from '../common/swagger/responses';
import { UsersService } from './users.service';
import {
	CreateUserDto,
	UpdateUserDto,
	UserDto,
	UserPaginationDto,
} from './users.dtos';
import { PasswordsService } from '../passwords/passwords.service';
import { RolesGuard } from '../auth/auth.guards';
import { HttpJwtAuth, Roles } from '../auth/auth.decorators';
import { User } from './users.interfaces';

@ApiTags('Users')
@Controller('users')
export class UsersController {
	constructor(
		readonly mail: MailService,
		readonly users: UsersService,
		readonly passwords: PasswordsService,
	) {}

	@Get()
	@Roles(UserRole.ADMIN)
	@HttpJwtAuth(RolesGuard)
	@ApiOperation({
		summary: 'Get the users',
		description: 'Get the users.',
		operationId: 'findMany',
	})
	@ApiQuery(CursorQuery)
	@ApiQuery(TakeQuery)
	@ApiQuery(FilterByFirstNameQuery)
	@ApiQuery(FilterByLastNameQuery)
	@ApiQuery(FilterByEmailQuery)
	@ApiQuery(FilterByUserStatusQuery)
	@ApiOkResponse({
		description: 'Users have been successfully retrieved.',
		type: () => UserPaginationDto,
	})
	@ApiBadRequestResponse(InvalidQueriesValuesResponse)
	async findMany(
		@Query('cursor') cursor: string,
		@Query('take', new CustomParseIntPipe({ optional: true })) take: number,
		@Query(
			'firstName',
			new ParseArrayPipe({
				items: String,
				optional: true,
			}),
		)
		firstName: string[],
		@Query(
			'lastName',
			new ParseArrayPipe({
				items: String,
				optional: true,
			}),
		)
		lastName: string[],
		@Query(
			'email',
			new ParseArrayPipe({
				items: String,
				optional: true,
			}),
		)
		email: string[],
		@Query(
			'userStatus',
			new ParseArrayPipe({
				items: String,
				optional: true,
			}),
		)
		userStatus: UserStatus[],
	): Promise<UserPaginationDto> {
		const where: Prisma.UserWhereInput[] = [];
		const params: Map<string, string[]> = new Map();

		if (firstName) {
			where.push({
				firstName: {
					in: firstName,
				},
			});

			params.set('firstName', firstName);
		}

		if (lastName) {
			where.push({
				lastName: {
					in: lastName,
				},
			});

			params.set('lastName', lastName);
		}

		if (email) {
			where.push({
				email: {
					in: email,
				},
			});

			params.set('email', email);
		}

		if (userStatus) {
			where.push({
				status: {
					in: userStatus,
				},
			});

			params.set('userStatus', userStatus);
		}

		const requestPagination = await this.users.findMany({
			cursor: cursor ? { id: cursor } : undefined,
			take,
			skip: cursor ? 1 : 0,
			orderBy: [
				{ updatedAt: 'desc' },
				{ lastName: 'asc' },
				{ firstName: 'asc' },
				{ email: 'asc' },
			],
			where: {
				AND: where,
			},
		});

		return UserPaginationDto.toDto(
			(requestPagination as unknown) as Pagination<User>,
			take,
			params,
		);
	}

	@Post()
	@HttpJwtAuth(RolesGuard)
	@Roles(UserRole.ADMIN)
	@ApiOperation({
		summary: 'Create a new user',
		description: 'Create a new user.',
		operationId: 'create',
	})
	@ApiBody({
		description: "The user's details.",
		type: () => CreateUserDto,
	})
	@ApiCreatedResponse({
		description: 'User has been successfully created.',
		type: () => UserDto,
	})
	@ApiConflictResponse({
		description:
			'Another user has the same email. Please try again with another email.',
	})
	@ApiBadRequestResponse(MissingOrIncorrectFieldsResponse)
	async create(@Body() createUserDto: CreateUserDto): Promise<UserDto> {
		const user = await this.users.create({
			data: {
				...createUserDto,
				password: await hash(nanoid()), // Random password
				status: UserStatus.ENABLED,
			},
		});

		try {
			const resetPasswordRequestId = await this.passwords.request(user.email);
			await this.mail.sendAccountCreated(user, resetPasswordRequestId);
		} catch (error) {
			await this.users.delete({
				where: {
					id: user.id,
				},
			});

			throw error;
		}

		return UserDto.toDto((user as unknown) as User);
	}

	@Get(':id')
	@Roles(UserRole.ADMIN)
	@HttpJwtAuth(RolesGuard)
	@ApiOperation({
		summary: 'Get the specified user',
		description: 'Get the specified user.',
		operationId: 'findFirst',
	})
	@ApiParam({
		name: 'id',
		description: 'The user ID.',
	})
	@ApiOkResponse({
		description: 'User has been successfully retrieved.',
		type: () => UserDto,
	})
	@ApiNotFoundResponse({
		description: 'User has not been found.',
	})
	@ApiBadRequestResponse(MissingOrIncorrectFieldsResponse)
	async findFirst(@Param('id', ParseUUIDPipe) id: string): Promise<UserDto> {
		const user = await this.users.findFirst({
			where: {
				id: {
					equals: id,
				},
			},
		});

		return UserDto.toDto((user as unknown) as User);
	}

	@Patch(':id')
	@Roles(UserRole.ADMIN)
	@HttpJwtAuth(RolesGuard)
	@ApiOperation({
		summary: 'Update the specified user',
		description: 'Update the specified user.',
		operationId: 'update',
	})
	@ApiParam({
		name: 'id',
		description: 'The user ID.',
	})
	@ApiBody({
		description: "The user's details.",
		type: () => UpdateUserDto,
	})
	@ApiOkResponse({
		description: 'User has been successfully updated.',
		type: () => UserDto,
	})
	@ApiNotFoundResponse({
		description: 'User has not been found.',
	})
	@ApiConflictResponse({
		description:
			'Another user has the same email. Please try again with another email.',
	})
	@ApiBadRequestResponse(MissingOrIncorrectFieldsResponse)
	async update(
		@Param('id', ParseUUIDPipe) id: string,
		@Body() updateUserDto: UpdateUserDto,
	): Promise<UserDto> {
		const user = await this.users.update({
			where: {
				id,
			},
			data: {
				...updateUserDto,
			},
		});

		return UserDto.toDto((user as unknown) as User);
	}

	@Delete(':id')
	@HttpCode(204)
	@Roles(UserRole.ADMIN)
	@HttpJwtAuth(RolesGuard)
	@ApiOperation({
		summary: 'Delete the specified user',
		description: 'Delete the specified user.',
		operationId: 'delete',
	})
	@ApiNoContentResponse({
		description: 'User has been successfully deleted.',
	})
	@ApiNotFoundResponse({
		description: 'User has not been found.',
	})
	async delete(@Param('id', ParseUUIDPipe) id: string): Promise<void> {
		await this.passwords.delete({
			where: {
				userId: id,
			},
		});

		await this.users.delete({
			where: {
				id,
			},
		});
	}
}
