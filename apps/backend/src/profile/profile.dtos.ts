import { ApiProperty, OmitType } from '@nestjs/swagger';
import { Language, UserGender } from '@prisma/client';
import { IsString, MinLength } from 'class-validator';
import { MININUM_PASSWORD_LENGTH } from '../constants';
import { AbstractProfile, Profile, UpdateProfile } from './profile.interfaces';
import { AbstractUserDto } from '../users/users.dtos';

export class AbstractProfileDto
	extends OmitType(AbstractUserDto, [
		'id',
		'password',
		'role',
		'status',
		'createdAt',
		'updatedAt',
	] as const)
	implements AbstractProfile {
	/**
	 * your current password
	 */
	@ApiProperty({ format: 'password' })
	@IsString()
	@MinLength(MININUM_PASSWORD_LENGTH)
	currentPassword!: string;

	/**
	 * your new password
	 */
	@ApiProperty({ format: 'password' })
	@IsString()
	@MinLength(MININUM_PASSWORD_LENGTH)
	newPassword!: string;
}

export class ProfileDto
	extends OmitType(AbstractProfileDto, [
		'currentPassword',
		'newPassword',
	] as const)
	implements Profile {
	constructor(
		readonly firstName: string,
		readonly lastName: string,
		readonly email: string,
		readonly language: Language,
		readonly birthDate: Date,
		readonly gender: UserGender,
		readonly mainSport: string,
	) {
		super();
	}

	static toDto(profile: Profile): ProfileDto {
		return new this(
			profile.firstName,
			profile.lastName,
			profile.email,
			profile.language,
			profile.birthDate,
			profile.gender,
			profile.mainSport,
		);
	}
}

export class UpdateProfileDto
	extends OmitType(AbstractProfileDto, ['email'] as const)
	implements UpdateProfile {}
