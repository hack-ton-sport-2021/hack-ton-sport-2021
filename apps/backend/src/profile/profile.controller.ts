import {
	Body,
	Controller,
	Delete,
	Get,
	Patch,
	UnauthorizedException,
} from '@nestjs/common';
import {
	ApiBadRequestResponse,
	ApiBody,
	ApiConflictResponse,
	ApiNoContentResponse,
	ApiOkResponse,
	ApiOperation,
	ApiTags,
} from '@nestjs/swagger';
import { User as UserModel, UserRole } from '@prisma/client';
import { hash, verify } from 'argon2';
import { MissingOrIncorrectFieldsResponse } from '../common/swagger/responses';
import { ProfileDto, UpdateProfileDto } from './profile.dtos';
import { RolesGuard } from '../auth/auth.guards';
import { HttpJwtAuth, Roles } from '../auth/auth.decorators';
import { User } from '../users/users.decorators';
import { UsersService } from '../users/users.service';

@ApiTags('Profile')
@Controller('profile')
export class ProfileController {
	constructor(readonly users: UsersService) {}

	@Get()
	@HttpJwtAuth(RolesGuard)
	@Roles(UserRole.ADMIN, UserRole.USER)
	@ApiOperation({
		summary: "Get the user's profile",
		description: "Get the user's profile",
		operationId: 'findFirst',
	})
	@ApiOkResponse({
		description: 'User has been successfully retrieved.',
		type: () => ProfileDto,
	})
	@ApiBadRequestResponse(MissingOrIncorrectFieldsResponse)
	// eslint-disable-next-line class-methods-use-this
	async findFirst(@User() user: UserModel): Promise<ProfileDto> {
		return ProfileDto.toDto(user);
	}

	@Patch()
	@HttpJwtAuth(RolesGuard)
	@Roles(UserRole.ADMIN, UserRole.USER)
	@ApiOperation({
		summary: "Update the user's profile",
		description: "Update the user's profile",
		operationId: 'update',
	})
	@ApiBody({
		description: "The profile's details.",
		type: () => UpdateProfileDto,
	})
	@ApiOkResponse({
		description: 'Profile has been successfully updated.',
		type: () => ProfileDto,
	})
	@ApiConflictResponse({
		description:
			'Another user has the same email. Please try again with another email.',
	})
	@ApiBadRequestResponse(MissingOrIncorrectFieldsResponse)
	async update(
		@User() user: UserModel,
		@Body() updateProfileDto: UpdateProfileDto,
	): Promise<ProfileDto> {
		if (await verify(user.password, updateProfileDto.currentPassword)) {
			const updatedUser = await this.users.update({
				data: {
					firstName: updateProfileDto.firstName,
					lastName: updateProfileDto.lastName,
					password: await hash(updateProfileDto.newPassword),
					language: updateProfileDto.language,
					birthDate: updateProfileDto.birthDate,
					gender: updateProfileDto.gender,
					mainSport: updateProfileDto.mainSport,
				},
				where: {
					id: user.id,
				},
			});

			return ProfileDto.toDto(updatedUser);
		}
		throw new UnauthorizedException();
	}

	@Delete()
	@HttpJwtAuth(RolesGuard)
	@Roles(UserRole.ADMIN, UserRole.USER)
	@ApiOperation({
		summary: 'Delete the user',
		description: 'Delete the user.',
		operationId: 'delete',
	})
	@ApiNoContentResponse({
		description: 'User has been successfully deleted.',
	})
	async delete(@User() user: UserModel): Promise<void> {
		await this.users.delete({
			where: {
				id: user.id,
			},
		});
	}
}
