import { AbstractUser } from '../users/users.interfaces';

export type AbstractProfile = Omit<
	AbstractUser,
	'id' | 'password' | 'study' | 'role' | 'status' | 'createdAt' | 'updatedAt'
> & {
	readonly currentPassword: string;
	readonly newPassword: string;
};

export type Profile = Omit<AbstractProfile, 'currentPassword' | 'newPassword'>;

export type UpdateProfile = Omit<AbstractProfile, 'email'>;
