import { AuthGuard } from '@nestjs/passport';
import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { User as UserModel } from '@prisma/client';
import { Request } from 'express';
import {
	HTTP_JWT_STRATEGY,
	HTTP_LOCAL_STRATEGY,
	HTTP_RESET_PASSWORD_STRATEGY,
} from '../constants';

@Injectable()
export class HttpJwtGuard extends AuthGuard(HTTP_JWT_STRATEGY) {}

@Injectable()
export class HttpLocalGuard extends AuthGuard(HTTP_LOCAL_STRATEGY) {}

@Injectable()
export class HttpResetPasswordGuard extends AuthGuard(
	HTTP_RESET_PASSWORD_STRATEGY,
) {}

@Injectable()
export class RolesGuard implements CanActivate {
	constructor(private reflector: Reflector) {}

	async canActivate(context: ExecutionContext): Promise<boolean> {
		const roles = this.reflector.get<string[]>('roles', context.getHandler());

		if (!roles) {
			return true;
		}

		const request: Request = context.switchToHttp().getRequest();

		const user = request.user as UserModel;

		return roles.includes(user.role);
	}
}
