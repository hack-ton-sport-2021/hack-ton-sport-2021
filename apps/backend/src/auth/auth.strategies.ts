import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt';
import { Strategy as LocalStrategy } from 'passport-local';
import { HeaderAPIKeyStrategy as ApiKeyStrategy } from 'passport-headerapikey';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { User as UserModel } from '@prisma/client';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';
import { CredentialsDto } from './auth.dtos';
import { AuthService } from './auth.service';
import { JwtDto } from '../jwts/jwts.dtos';
import {
	HTTP_JWT_STRATEGY,
	HTTP_LOCAL_STRATEGY,
	HTTP_RESET_PASSWORD_STRATEGY,
	JWT_SECRET,
} from '../constants';

@Injectable()
export class HttpJwtStrategy extends PassportStrategy(
	JwtStrategy,
	HTTP_JWT_STRATEGY,
) {
	constructor(readonly auth: AuthService, readonly config: ConfigService) {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			ignoreExpiration: false,
			secretOrKey: config.get<string>(JWT_SECRET),
		});
	}

	async validate(jwtDto: JwtDto): Promise<UserModel> {
		try {
			const user = await this.auth.validateUser(jwtDto.data.userId);

			return user;
		} catch (error) {
			throw new UnauthorizedException();
		}
	}
}

@Injectable()
export class HttpLocalStrategy extends PassportStrategy(
	LocalStrategy,
	HTTP_LOCAL_STRATEGY,
) {
	constructor(readonly auth: AuthService) {
		super({
			usernameField: 'email',
			passwordField: 'password',
		});
	}

	async validate(email: string, password: string): Promise<UserModel> {
		try {
			const user = await this.auth.validateCredentials(
				new CredentialsDto(email, password),
			);

			return user;
		} catch (error) {
			throw new UnauthorizedException();
		}
	}
}

@Injectable()
export class HttpResetPasswordStrategy extends PassportStrategy(
	ApiKeyStrategy,
	HTTP_RESET_PASSWORD_STRATEGY,
) {
	constructor(readonly auth: AuthService) {
		super(
			{
				header: 'Password-Reset',
			},
			true,
			async (
				passwordRequestId: string,
				verified: (err: Error | null, user?: UserModel) => void,
				req: Request,
			) => {
				try {
					const userReq = await this.validate(passwordRequestId);

					req.user = userReq || null;

					return verified(null, userReq);
				} catch (error) {
					return verified(error);
				}
			},
		);
	}

	async validate(passwordRequestId: string): Promise<UserModel> {
		try {
			const user = await this.auth.validatePasswordResetRequest(
				passwordRequestId,
			);

			return user;
		} catch (error) {
			throw new UnauthorizedException();
		}
	}
}
