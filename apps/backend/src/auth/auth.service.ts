import { Injectable } from '@nestjs/common';
import { verify } from 'argon2';
import { User, UserStatus } from '@prisma/client';
import { CredentialsDto } from './auth.dtos';
import { UsersService } from '../users/users.service';
import { JwtsService } from '../jwts/jwts.service';
import { JwtDataDto } from '../jwts/jwts.dtos';
import { PasswordsService } from '../passwords/passwords.service';

@Injectable()
export class AuthService {
	constructor(
		readonly users: UsersService,
		readonly jsonWebTokens: JwtsService,
		readonly passwords: PasswordsService,
	) {}

	async validateCredentials(credentials: CredentialsDto): Promise<User> {
		const user = await this.users.findFirst({
			where: {
				email: credentials.email,
			},
		});

		if (
			user &&
			user.password &&
			(await verify(user.password, credentials.password)) &&
			user.status !== UserStatus.DISABLED
		) {
			return user;
		}

		throw new Error();
	}

	async validateUser(userId: string): Promise<User> {
		return this.users.findFirst({
			where: {
				id: userId,
			},
		});
	}

	async validatePasswordResetRequest(passwordRequestId: string): Promise<User> {
		const passwordRequest = await this.passwords.findFirst({
			id: passwordRequestId,
		});

		const user = await this.users.findFirst({
			where: {
				id: passwordRequest.userId,
			},
		});

		if (user) {
			return user;
		}

		throw new Error();
	}

	async logIn(user: User): Promise<string> {
		const jwtDataDto = new JwtDataDto(user.id, user.firstName, user.role);

		return this.jsonWebTokens.sign(jwtDataDto);
	}
}
