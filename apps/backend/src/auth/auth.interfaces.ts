import { User as UserModel } from '@prisma/client';

export interface Credentials {
	readonly email: string;
	readonly password: string;
}

export interface ResetPasswordRequest {
	readonly email: string;
}

export interface ResetPassword {
	readonly password: string;
}

export interface LoggedIn {
	readonly jwt: string;
}
