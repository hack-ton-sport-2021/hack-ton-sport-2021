import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MinLength } from 'class-validator';
import { MININUM_PASSWORD_LENGTH } from '../constants';
import {
	Credentials,
	LoggedIn,
	ResetPassword,
	ResetPasswordRequest,
} from './auth.interfaces';

export class CredentialsDto implements Credentials {
	/**
	 * email of the user to log in
	 */
	@ApiProperty({ format: 'email' })
	@IsEmail()
	readonly email: string;

	/**
	 * password of the user to login
	 */
	@ApiProperty({ format: 'password' })
	@IsString()
	readonly password: string;

	constructor(email: string, password: string) {
		this.email = email;
		this.password = password;
	}
}

export class ResetPasswordRequestDto implements ResetPasswordRequest {
	/**
	 * email of the user to reset password
	 */
	@ApiProperty({ format: 'email' })
	@IsEmail()
	readonly email: string;

	constructor(email: string) {
		this.email = email;
	}
}

export class ResetPasswordDto implements ResetPassword {
	/**
	 * the new password of the user
	 */
	@ApiProperty({ format: 'password' })
	@IsString()
	@MinLength(MININUM_PASSWORD_LENGTH)
	readonly password: string;

	constructor(password: string) {
		this.password = password;
	}
}

export class LoggedInDto implements LoggedIn {
	/**
	 * jwt to access protected endpoints
	 */
	@ApiProperty({ format: 'jwt' })
	@IsString()
	readonly jwt: string;

	constructor(jwt: string) {
		this.jwt = jwt;
	}
}
