import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import {
	ApiBody,
	ApiNoContentResponse,
	ApiOkResponse,
	ApiOperation,
	ApiTags,
	ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { User as UserModel, UserRole } from '@prisma/client';
import { AuthService } from './auth.service';
import { User } from '../users/users.decorators';
import {
	CredentialsDto,
	LoggedInDto,
	ResetPasswordDto,
	ResetPasswordRequestDto,
} from './auth.dtos';
import {
	HttpJwtAuth,
	HttpLocalAuth,
	HttpResetPasswordAuth,
	Roles,
} from './auth.decorators';
import { HttpResetPasswordGuard, RolesGuard } from './auth.guards';
import { PasswordsService } from '../passwords/passwords.service';
import { MailService } from '../mail/mail.service';
import { UsersService } from '../users/users.service';

@ApiTags('Auth')
@Controller()
export class AuthController {
	constructor(
		readonly auth: AuthService,
		readonly mail: MailService,
		readonly passwords: PasswordsService,
		readonly users: UsersService,
	) {}

	@Post('log-in')
	@HttpLocalAuth()
	@HttpCode(200)
	@ApiOperation({
		summary: 'Log in to the backend',
		description: 'Log in to the backend.',
		operationId: 'logIn',
	})
	@ApiBody({
		description: "The user's credentials.",
		type: () => CredentialsDto,
	})
	@ApiOkResponse({
		description: 'The user has been successfully logged in.',
		type: () => LoggedInDto,
	})
	@ApiUnauthorizedResponse({
		description: 'Wrong username/password.',
	})
	async logIn(@User() user: UserModel): Promise<LoggedInDto> {
		const jwt = await this.auth.logIn(user);

		return new LoggedInDto(jwt);
	}

	@Post('log-out')
	@HttpCode(204)
	@HttpJwtAuth(RolesGuard)
	@Roles(UserRole.ADMIN, UserRole.USER)
	@ApiOperation({
		summary: 'Log out from the backend',
		description: 'Log out from the backend.',
		operationId: 'logOut',
	})
	@ApiNoContentResponse({
		description: 'The user has been successfully logged out.',
	})
	async logOut(): Promise<void> {
		// Do nothing at the moment
	}

	@Post('reset-password-request')
	@HttpCode(204)
	@ApiOperation({
		summary: "Request to reset the user's password",
		description: "Request to reset the user's password.",
		operationId: 'resetPasswordRequest',
	})
	@ApiBody({
		description: "The user's email.",
		type: () => ResetPasswordRequestDto,
	})
	@ApiNoContentResponse({
		description:
			"The user's password reset request has been sucessfully filled.",
	})
	async resetPasswordRequest(
		@Body() resetPasswordDto: ResetPasswordRequestDto,
	): Promise<void> {
		try {
			const requestId = await this.passwords.request(resetPasswordDto.email);

			const user = await this.users.findFirst({
				where: {
					email: resetPasswordDto.email,
				},
			});

			await this.mail.sendResetPasswordRequest(user, requestId);
		} catch (error) {
			// Do nothing
		}
	}

	@Post('reset-password')
	@HttpCode(204)
	@HttpResetPasswordAuth(HttpResetPasswordGuard)
	@ApiOperation({
		summary: "Reset the user's password",
		description: "Reset the user's password.",
		operationId: 'resetPassword',
	})
	@ApiBody({
		description: "The user's new password.",
		type: () => ResetPasswordDto,
	})
	@ApiNoContentResponse({
		description: "The user's password has been successfully reset.",
	})
	async resetPassword(
		@User() user: UserModel,
		@Body() resetPasswordDto: ResetPasswordDto,
	): Promise<void> {
		await this.passwords.reset(user, resetPasswordDto.password);
	}
}
