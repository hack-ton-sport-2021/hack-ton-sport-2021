import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PrismaModule } from '../prisma/prisma.module';
import { UsersModule } from '../users/users.module';
import { AuthController } from './auth.controller';
import {
	HttpJwtStrategy,
	HttpLocalStrategy,
	HttpResetPasswordStrategy,
} from './auth.strategies';
import { PasswordsModule } from '../passwords/passwords.module';
import { AuthService } from './auth.service';
import { JwtsModule } from '../jwts/jwts.module';
import { MailModule } from '../mail/mail.module';

@Module({
	imports: [
		ConfigModule,
		JwtsModule,
		MailModule,
		PasswordsModule,
		PrismaModule,
		UsersModule,
	],
	controllers: [AuthController],
	providers: [
		AuthService,
		HttpJwtStrategy,
		HttpLocalStrategy,
		HttpResetPasswordStrategy,
	],
})
export class AuthModule {}
