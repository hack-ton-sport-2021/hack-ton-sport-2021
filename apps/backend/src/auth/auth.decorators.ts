/* eslint-disable @typescript-eslint/ban-types */
import {
	CanActivate,
	SetMetadata,
	UseGuards,
	applyDecorators,
} from '@nestjs/common';
import {
	ApiBearerAuth,
	ApiExtension,
	ApiForbiddenResponse,
	ApiNotAcceptableResponse,
	ApiQuery,
	ApiSecurity,
	ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { UserQuery } from '../common/swagger/queries';
import {
	HttpJwtGuard,
	HttpLocalGuard,
	HttpResetPasswordGuard,
} from './auth.guards';

export const HttpJwtAuth = (
	...guards: (Function | CanActivate)[]
): (<TFunction extends Function, Y>(
	target: object | TFunction,
	propertyKey?: string | symbol | undefined,
	descriptor?: TypedPropertyDescriptor<Y> | undefined,
) => void) =>
	applyDecorators(
		UseGuards(HttpJwtGuard, ...guards),
		ApiBearerAuth(),
		ApiUnauthorizedResponse({
			description: 'Wrong authentication jwt.',
		}),
		ApiForbiddenResponse({
			description: 'Unsufficient roles or permissions.',
		}),
	);

export const HttpLocalAuth = (
	...guards: (Function | CanActivate)[]
): (<TFunction extends Function, Y>(
	target: object | TFunction,
	propertyKey?: string | symbol | undefined,
	descriptor?: TypedPropertyDescriptor<Y> | undefined,
) => void) =>
	applyDecorators(
		UseGuards(HttpLocalGuard, ...guards),
		ApiUnauthorizedResponse({
			description: 'Wrong username/password.',
		}),
		ApiForbiddenResponse({
			description: 'Unsufficient roles or permissions.',
		}),
	);

export const HttpResetPasswordAuth = (
	...guards: (Function | CanActivate)[]
): (<TFunction extends Function, Y>(
	target: object | TFunction,
	propertyKey?: string | symbol | undefined,
	descriptor?: TypedPropertyDescriptor<Y> | undefined,
) => void) =>
	applyDecorators(
		UseGuards(HttpResetPasswordGuard, ...guards),
		ApiSecurity('password_reset'),
		ApiUnauthorizedResponse({
			description: 'Wrong Password Reset ID.',
		}),
		ApiForbiddenResponse({
			description: 'Unsufficient roles or permissions.',
		}),
	);

export const Roles = (
	...roles: string[]
): // eslint-disable-next-line @typescript-eslint/ban-types
(<TFunction extends Function, Y>(
	// eslint-disable-next-line @typescript-eslint/ban-types
	target: object | TFunction,
	propertyKey?: string | symbol | undefined,
	descriptor?: TypedPropertyDescriptor<Y> | undefined,
) => void) =>
	applyDecorators(
		ApiQuery(UserQuery),
		SetMetadata('roles', roles),
		ApiExtension('x-roles', `[${roles.join(', ')}]`),
	);

export const Permissions = (
	...permissions: string[]
): // eslint-disable-next-line @typescript-eslint/ban-types
(<TFunction extends Function, Y>(
	// eslint-disable-next-line @typescript-eslint/ban-types
	target: object | TFunction,
	propertyKey?: string | symbol | undefined,
	descriptor?: TypedPropertyDescriptor<Y> | undefined,
) => void) =>
	applyDecorators(
		SetMetadata('permissions', permissions),
		ApiExtension('x-permissions', `[${permissions.join()}]`),
		ApiNotAcceptableResponse({
			description:
				'Request not acceptable. You cannot update your own permissions or you are not the owner of the project.',
		}),
	);
