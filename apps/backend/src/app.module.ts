import { Module, ModuleMetadata } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { MailModule } from './mail/mail.module';
import { PasswordsModule } from './passwords/passwords.module';
import { PrismaModule } from './prisma/prisma.module';
import { ProfileModule } from './profile/profile.module';

export const moduleMetadata: ModuleMetadata = {
	imports: [
		AuthModule,
		ConfigModule,
		MailModule,
		PasswordsModule,
		PrismaModule,
		ProfileModule,
	],
};

@Module(moduleMetadata)
export class AppModule {}
