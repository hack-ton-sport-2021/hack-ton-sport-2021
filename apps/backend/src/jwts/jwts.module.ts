import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { JWT_EXPIRATION_TIME, JWT_SECRET } from '../constants';
import { PrismaModule } from '../prisma/prisma.module';
import { JwtsService } from './jwts.service';

@Module({
	imports: [
		ConfigModule,
		JwtModule.registerAsync({
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: async (config: ConfigService) => ({
				secret: config.get<string>(JWT_SECRET),
				signOptions: {
					expiresIn: config.get<string>(JWT_EXPIRATION_TIME),
				},
			}),
		}),
		PrismaModule,
	],
	providers: [JwtsService],
	exports: [JwtsService],
})
export class JwtsModule {}
