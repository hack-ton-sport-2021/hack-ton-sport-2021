import { UserRole } from '@prisma/client';
import { Jwt, JwtData } from './jwts.interfaces';

export class JwtDataDto implements JwtData {
	readonly userId: string;

	readonly userFirstName: string;

	readonly userRole: UserRole;

	constructor(userId: string, userFirstName: string, userRole: UserRole) {
		this.userId = userId;
		this.userFirstName = userFirstName;
		this.userRole = userRole;
	}
}

export class JwtDto implements Jwt {
	readonly iat: number;

	readonly jti: string;

	readonly data: JwtDataDto;

	constructor(iat: number, jti: string, data: JwtDataDto) {
		this.iat = iat;
		this.jti = jti;
		this.data = data;
	}
}
