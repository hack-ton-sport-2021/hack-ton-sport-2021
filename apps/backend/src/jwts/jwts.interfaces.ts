import { UserRole } from '@prisma/client';

export interface JwtData {
	readonly userId: string;
	readonly userFirstName: string;
	readonly userRole: UserRole;
}

export interface Jwt {
	readonly iat: number;
	readonly jti: string;
	readonly data: JwtData;
}
