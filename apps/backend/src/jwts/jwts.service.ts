import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { JWT_ISSUER } from '../constants';
import { PrismaService } from '../prisma/prisma.service';
import { JwtDataDto } from './jwts.dtos';

@Injectable()
export class JwtsService {
	constructor(
		readonly config: ConfigService,
		readonly jwt: JwtService,
		readonly prisma: PrismaService,
	) {}

	async sign(jwtDataDto: JwtDataDto): Promise<string> {
		return this.jwt.sign(
			{ data: jwtDataDto },
			{
				issuer: this.config.get<string>(JWT_ISSUER),
			},
		);
	}
}
