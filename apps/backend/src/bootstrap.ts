import compression from 'compression';
import { ConfigService } from '@nestjs/config';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { VERSION } from './constants';
import {
	PrismaClientKnownRequestExceptionsFilter,
	PrismaClientUnknownRequestExceptionsFilter,
	PrismaClientValidationExceptionsFilter,
} from './common/filters';
import { HttpAdapterHost } from '@nestjs/core';
import { SwitchUser } from './common/interceptors';
import { UsersService } from './users/users.service';

export const bootstrap = async (
	app: INestApplication,
): Promise<INestApplication> => {
	const config = app.get(ConfigService);
	const users = app.get(UsersService);
	const { httpAdapter } = app.get(HttpAdapterHost);

	app.useGlobalPipes(
		new ValidationPipe({
			transform: true,
		}),
	);

	app.useGlobalFilters(
		new PrismaClientKnownRequestExceptionsFilter(httpAdapter),
		new PrismaClientUnknownRequestExceptionsFilter(httpAdapter),
		new PrismaClientValidationExceptionsFilter(httpAdapter),
	);

	app.useGlobalInterceptors(new SwitchUser(users));

	app.enableCors();

	const swaggerConfig = new DocumentBuilder()
		.setTitle('Hack-ton-Sport API')
		.setDescription(
			'This is the API used during the Hack-ton-Sport 2021 hackathon made at the HEIG-VD, Switzerland.',
		)
		.setContact(
			'SPORTIWorld team',
			'https://gitlab.com/hack-ton-sport-2021/hack-ton-sport-2021',
			'incoming+hack-ton-sport-2021-hack-ton-sport-2021-25916734-issue-@incoming.gitlab.com',
		)
		.setVersion(config.get<string>(VERSION) as string)
		.setExternalDoc(
			'Additional Documentation',
			'https://gitlab.com/hack-ton-sport-2021/hack-ton-sport-2021#readme',
		)
		.addBearerAuth(
			{
				type: 'http',
				description: 'Authentication for users using Bearer JWT',
			},
			'bearer',
		)
		.addApiKey(
			{
				type: 'apiKey',
				description: 'Authentication for password reset',
				name: 'Password-Reset',
				in: 'header',
			},
			'password_reset',
		)
		.build();

	const document = SwaggerModule.createDocument(app, swaggerConfig, {
		extraModels: [],
	});

	SwaggerModule.setup('/', app, document, {
		swaggerOptions: {
			showExtensions: true,
			tagsSorter: 'alpha',
		},
	});

	app.use(compression());

	return app;
};
