module.exports = {
	testEnvironment: 'node',
	preset: 'ts-jest',
	testMatch: ['<rootDir>/**/?(*.)+(spec|steps|test).ts'],
	coverageDirectory: 'coverage',
	testTimeout: 120000,
	globals: {
		'ts-jest': {
			astTransformers: {
				before: ['jest-swagger.config.js'],
			},
		},
	},
};
