import util from 'util';
import { exec } from 'child_process';
import { PrismaClient } from '@prisma/client';
import { Client } from 'pg';
import { nanoid } from 'nanoid';

const ON_DIFFERENT_SCHEMA = true;
const DROP_SCHEMA = true;

const asyncExec = util.promisify(exec);

export interface Prisma {
	prismaClient: PrismaClient;
	schemaId: string;
}

export const setupPrisma = async (): Promise<Prisma> => {
	// Create the environment variable
	const schemaId = `test-${nanoid()}`;

	if (ON_DIFFERENT_SCHEMA) {
		process.env.HACK_TON_SPORT_BACKEND_DB_URL = process.env.HACK_TON_SPORT_BACKEND_DB_URL?.replace(
			'?schema=public',
			`?schema=${schemaId}`,
		);
	}

	// Run the migrations
	await asyncExec('npm run prisma:migrate:deploy', {
		env: process.env,
	});

	const prismaClient = new PrismaClient();

	return {
		prismaClient,
		schemaId,
	};
};

export const teardownPrisma = async (prisma: Prisma): Promise<void> => {
	if (DROP_SCHEMA) {
		if (ON_DIFFERENT_SCHEMA) {
			process.env.HACK_TON_SPORT_BACKEND_DB_URL = process.env.HACK_TON_SPORT_BACKEND_DB_URL?.replace(
				`?schema=${prisma.schemaId}`,
				'?schema=public',
			);
		}

		const client = new Client({
			connectionString: process.env.HACK_TON_SPORT_BACKEND_DB_URL,
		});

		// Drop the schema
		await client.connect();
		await client.query(`DROP SCHEMA IF EXISTS "${prisma.schemaId}" CASCADE`);
		await client.end();

		await prisma.prismaClient.$disconnect();
	}
};
