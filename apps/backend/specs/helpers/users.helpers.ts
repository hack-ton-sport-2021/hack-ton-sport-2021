import request, { Response } from 'supertest';
import { CreateUserDto, UpdateUserDto } from '../../src/users/users.dtos';
import { Nest } from './nest';

export const getAllUsers = async (
	nest: Nest,
	jwt: string,
	params?: string,
): Promise<Response> => {
	const url = params ? `/users?${params}` : '/users';

	const res = await request(nest.server)
		.get(url)
		.set('Authorization', `Bearer ${jwt}`);

	return res;
};

export const createUser = async (
	nest: Nest,
	jwt: string,
	user: CreateUserDto,
): Promise<Response> => {
	const res = await request(nest.server)
		.post('/users')
		.set('Authorization', `Bearer ${jwt}`)
		.send(user);

	return res;
};

export const getUser = async (
	nest: Nest,
	jwt: string,
	id: string,
	params?: string,
): Promise<Response> => {
	const url = params ? `/users/${id}?${params}` : `/users/${id}`;

	const res = await request(nest.server)
		.get(url)
		.set('Authorization', `Bearer ${jwt}`);

	return res;
};

export const updateUser = async (
	nest: Nest,
	jwt: string,
	id: string,
	user: UpdateUserDto,
): Promise<Response> => {
	const res = await request(nest.server)
		.patch(`/users/${id}`)
		.set('Authorization', `Bearer ${jwt}`)
		.send(user);

	return res;
};

export const deleteUser = async (
	nest: Nest,
	jwt: string,
	id: string,
): Promise<Response> => {
	const res = await request(nest.server)
		.delete(`/users/${id}`)
		.set('Authorization', `Bearer ${jwt}`);

	return res;
};
