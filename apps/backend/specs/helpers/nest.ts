import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { moduleMetadata } from '../../src/app.module';
import { bootstrap } from '../../src/bootstrap';

export interface Nest {
	app: INestApplication;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	server: any;
}

export const setupNest = async (): Promise<Nest> => {
	const moduleFixture: TestingModule = await Test.createTestingModule(
		moduleMetadata,
	).compile();

	let app = moduleFixture.createNestApplication();

	app = await bootstrap(app);

	const server = app.getHttpServer();

	await app.init();

	return {
		app,
		server,
	};
};

export const teardownNest = async (nest: Nest): Promise<void> => {
	await nest.app.close();
};
