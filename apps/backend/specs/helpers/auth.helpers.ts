import request, { Response } from 'supertest';
import { CredentialsDto, ResetPasswordDto } from '../../src/auth/auth.dtos';
import { Nest } from './nest';

export const logIn = async (
	nest: Nest,
	credentials: CredentialsDto,
): Promise<Response> => {
	const res = await request(nest.server).post('/log-in').send(credentials);

	return res;
};

export const logOut = async (nest: Nest, jwt: string): Promise<Response> => {
	const res = await request(nest.server)
		.post('/log-out')
		.set('Authorization', `Bearer ${jwt}`);

	return res;
};

export const resetPassword = async (
	nest: Nest,
	requestId: string,
	password: ResetPasswordDto,
): Promise<Response> => {
	const res = await request(nest.server)
		.post('/reset-password')
		.set('Password-Reset', requestId)
		.send(password);

	return res;
};

export const logInWithDefaultCredentials = async (
	nest: Nest,
): Promise<Response> => {
	const credentials = new CredentialsDto(
		process.env.HACK_TON_SPORT_BACKEND_DEFAULT_ADMIN_EMAIL as string,
		process.env.HACK_TON_SPORT_BACKEND_DEFAULT_ADMIN_PASSWORD as string,
	);

	return logIn(nest, credentials);
};
