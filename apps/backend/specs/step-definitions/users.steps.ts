import { Response } from 'supertest';
import { defineFeature, loadFeature } from 'jest-cucumber';
import { Language, UserGender, UserRole } from '@prisma/client';
import { logInWithDefaultCredentials } from '../helpers/auth.helpers';
import {
	Nest,
	Prisma,
	setupNest,
	setupPrisma,
	teardownNest,
	teardownPrisma,
	createUser,
	deleteUser,
	getAllUsers,
	getUser,
	updateUser,
} from '../helpers';
import { CreateUserDto, UpdateUserDto } from '../../src/users/users.dtos';

const feature = loadFeature('./specs/features/users.feature');

defineFeature(feature, (test) => {
	let prisma: Prisma;
	let nest: Nest;

	const createUserDto = new CreateUserDto(
		'first name',
		'last name',
		'email@test.ch',
		Language.ENGLISH,
		new Date('2021-01-14T11:33:47.052Z'),
		UserGender.FEMALE,
		'Football',
		UserRole.USER,
	);

	const updateUserDto = new UpdateUserDto(
		'updated first name',
		'updated last name',
		'updatedemail@test.ch',
		Language.FRENCH,
		new Date('2021-02-14T11:33:47.052Z'),
		UserGender.MALE,
		'Basketball',
		UserRole.ADMIN,
	);

	test('Create a new user', ({ given, when, then }) => {
		let res: Response;
		let jwt: string;

		given('I am logged as the default admin', async () => {
			res = await logInWithDefaultCredentials(nest);
			jwt = res.body.jwt;
		});

		when('I create a new user', async () => {
			res = await createUser(nest, jwt, createUserDto);
		});

		then('the user should be created', async () => {
			expect(res.status).toEqual(201);
			expect(res.body).toMatchSnapshot({
				id: expect.any(String),
			});
		});
	});

	test('Get an user', ({ given, when, then }) => {
		let res: Response;
		let jwt: string;

		given('I am logged as the default admin', async () => {
			res = await logInWithDefaultCredentials(nest);
			jwt = res.body.jwt;
		});

		given('I have created a new user', async () => {
			res = await createUser(nest, jwt, createUserDto);
		});

		when('I get the new user', async () => {
			res = await getUser(nest, jwt, res.body.id);
		});

		then('the user should be retrieved', async () => {
			expect(res.status).toEqual(200);
			expect(res.body).toMatchSnapshot({
				id: expect.any(String),
			});
		});
	});

	test('Update an user', ({ given, when, then }) => {
		let res: Response;
		let jwt: string;

		given('I am logged as the default admin', async () => {
			res = await logInWithDefaultCredentials(nest);
			jwt = res.body.jwt;
		});

		given('I have created a new user', async () => {
			res = await createUser(nest, jwt, createUserDto);
		});

		when('I update the new user', async () => {
			res = await updateUser(nest, jwt, res.body.id, updateUserDto);
		});

		then('the user should be updated', async () => {
			expect(res.status).toEqual(200);
			expect(res.body).toMatchSnapshot({
				id: expect.any(String),
			});
		});
	});

	test('Delete an user', ({ given, when, then }) => {
		let res: Response;
		let jwt: string;

		given('I am logged as the default admin', async () => {
			res = await logInWithDefaultCredentials(nest);
			jwt = res.body.jwt;
		});

		given('I have created a new user', async () => {
			res = await createUser(nest, jwt, createUserDto);
		});

		when('I delete the new user', async () => {
			res = await deleteUser(nest, jwt, res.body.id);
		});

		then('the user should be deleted', async () => {
			expect(res.status).toEqual(204);
			expect(res.body).toMatchSnapshot();
		});
	});

	test('Get all users', ({ given, when, then }) => {
		let res: Response;
		let jwt: string;

		given('I am logged as the default admin', async () => {
			res = await logInWithDefaultCredentials(nest);
			jwt = res.body.jwt;
		});

		given('I have created a new user', async () => {
			res = await createUser(nest, jwt, createUserDto);
		});

		when('I ask all the users', async () => {
			res = await getAllUsers(nest, jwt, 'take=10');
		});

		then('the users should be retrieved', async () => {
			expect(res.status).toEqual(200);
			expect(res.body).toMatchSnapshot({
				items: [
					{
						id: expect.any(String),
					},
					{
						id: expect.any(String),
					},
				],
			});
		});
	});

	beforeEach(async () => {
		prisma = await setupPrisma();
		nest = await setupNest();
	});

	afterEach(async () => {
		await teardownNest(nest);
		await teardownPrisma(prisma);
	});
});
