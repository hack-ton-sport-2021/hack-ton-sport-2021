import { Response } from 'supertest';
import { defineFeature, loadFeature } from 'jest-cucumber';
import {
	Nest,
	Prisma,
	setupNest,
	setupPrisma,
	teardownNest,
	teardownPrisma,
	logInWithDefaultCredentials,
	logOut,
} from '../helpers';

const feature = loadFeature('./specs/features/auth.feature');

defineFeature(feature, (test) => {
	let prisma: Prisma;
	let nest: Nest;

	test('Log in as the default admin', ({ when, then }) => {
		let res: Response;

		when('I log in with the default credentials', async () => {
			res = await logInWithDefaultCredentials(nest);
		});

		then('I should have access to the system', async () => {
			expect(res.status).toEqual(200);
			expect(res.body.jwt).toBeDefined();
		});
	});

	test('Log out as the default admin', ({ given, when, then, and }) => {
		let res: Response;
		let jwt: string;

		given('I am logged in', async () => {
			res = await logInWithDefaultCredentials(nest);
			jwt = res.body.jwt;
		});

		when('I log out', async () => {
			res = await logOut(nest, jwt);
		});

		then('I should be logged out', async () => {
			expect(res.status).toEqual(204);
		});
	});

	beforeEach(async () => {
		prisma = await setupPrisma();
		nest = await setupNest();
	});

	afterEach(async () => {
		await teardownNest(nest);
		await teardownPrisma(prisma);
	});
});
