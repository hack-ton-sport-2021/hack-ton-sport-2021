Feature: Users

Scenario: Create a new user
	Given I am logged as the default admin
	When I create a new user
	Then the user should be created

Scenario: Get an user
	Given I am logged as the default admin
	Given I have created a new user
	When I get the new user
	Then the user should be retrieved

Scenario: Update an user
	Given I am logged as the default admin
	Given I have created a new user
	When I update the new user
	Then the user should be updated

Scenario: Delete an user
	Given I am logged as the default admin
	Given I have created a new user
	When I delete the new user
	Then the user should be deleted

Scenario: Get all users
	Given I am logged as the default admin
	Given I have created a new user
	When I ask all the users
	Then the users should be retrieved
