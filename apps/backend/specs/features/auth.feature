Feature: Auth

Scenario: Log in as the default admin
	When I log in with the default credentials
	Then I should have access to the system

Scenario: Log out as the default admin
	Given I am logged in
	When I log out
	Then I should be logged out
