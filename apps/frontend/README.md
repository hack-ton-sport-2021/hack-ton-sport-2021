# **Hack-ton-sport** - Frontend

## Prerequisites

The following prerequisites must be filled to run this project:

- [Node.js](https://nodejs.org/) must be installed.
- [pnpm](https://pnpm.js.org/) must be installed.

## Configuration

```sh
# Install the dependencies
pnpm install

# Copy the .env.defaults file to a .env file.
cp .env.defaults .env

# Edit the environment variables file if needed
vim .env
```

### Start the app in development mode

```sh
# Start the app in development mode
pnpm dev
```

### Build the app

```sh
# Build the app
pnpm build
```

### Start the app in production mode

```sh
# Start the app in production mode
pnpm start
```

## Additional resources

- [NuxtJS](https://nuxtjs.org/) - All the features you need for production: hybrid static & server rendering, TypeScript support, smart bundling, route pre-fetching, and more.
- [pnpm](https://pnpm.js.org/) - Fast, disk space efficient package manager.
- [TypeScript](https://www.typescriptlang.org/) - An open-source language which builds on JavaScript by adding static type definitions.
- [Vuetify](https://vuetifyjs.com) - A Vue UI Library with beautifully handcrafted Material Components.
